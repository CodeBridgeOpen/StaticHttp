package web

import (
	"github.com/elazarl/go-bindata-assetfs"
	"gitlab.com/CodeBridgeOpen/StaticHttp/helpers"
	"net/http"
	"os"
	"regexp"
)

var Prefix = "static_sources"

const defaultFile = "/index.html"

func Handler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Cache-Control", "public, max-age=3600")

	http.FileServer(
		&assetfs.AssetFS{
			Asset: func(path string) ([]byte, error) {
				out, err := Asset(path)

				helpers.Debug(path, err)

				if err != nil && isPathDirectory(path) && path != Prefix {
					w.Header().Set("Content-Type", "text/html; charset=utf-8")

					tryFolderIndex, err := Asset(path + defaultFile)
					if err == nil {
						return tryFolderIndex, err
					}

					return Asset(Prefix + defaultFile)
				}

				return out, err
			},
			AssetDir: AssetDir,
			AssetInfo: func(path string) (os.FileInfo, error) {
				return os.Stat(path)
			},
			Prefix: Prefix,
		}).ServeHTTP(w, r)
}

func isPathDirectory(path string) bool {
	var re = regexp.MustCompile(`(?m)\.\w{2,5}`)
	return re.FindString(path) == ""
}
