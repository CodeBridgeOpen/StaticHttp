package helpers

import "os"

func Env(v string) string {
	return os.Getenv(v)
}
