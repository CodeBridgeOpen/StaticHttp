package helpers

import "log"

func Debug(v ...interface{}) {
	debug := false

	if Env("STATIC_HTTP_DEBUG") == "true" {
		debug = true
	}

	if debug {
		log.Println(v...)
	}
}