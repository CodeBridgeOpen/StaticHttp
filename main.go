package main

//go:generate go-bindata -pkg web -o web/data.go static_sources/...

import (
	"gitlab.com/CodeBridgeOpen/StaticHttp/helpers"
	"gitlab.com/CodeBridgeOpen/StaticHttp/web"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", web.Handler)

	if err := serve(); err != nil {
		log.Fatal(err)
	}
}

func serve() error {
	port := helpers.Env("STATIC_HTTP_PORT")

	if port == "" {
		port = ":8080"
	}

	return http.ListenAndServe(port, nil)
}
